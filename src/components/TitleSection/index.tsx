import React from "react";
import * as Styled from "./styles";

export const TitleSection = ({ children = null, ...props }) => {
  return <Styled.Base {...props}>{children}</Styled.Base>;
};

TitleSection.displayName = "TitleSection";
