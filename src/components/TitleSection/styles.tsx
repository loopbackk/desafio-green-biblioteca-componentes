import styled from "styled-components";
import tokens from "~tokens";

export const Base = styled.h2`
  color: ${tokens.colorNeutralDark};
`;
