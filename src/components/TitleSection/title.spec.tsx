import React from "react";
import { render, screen } from "@testing-library/react";
import { TitleSection } from "./";

test("renderização correta do texto e estilo", async () => {
  render(<TitleSection data-testid={"titlesection"}>Título da seção</TitleSection>);

  const input = screen.getByTestId("titlesection");

  expect(input).toHaveTextContent("Título da seção");
  expect(input).toHaveStyle("color: rgb(93, 97, 95);");
});
