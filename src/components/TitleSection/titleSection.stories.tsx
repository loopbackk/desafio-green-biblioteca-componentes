import React from "react";
import { TitleSection } from "./index";

export default { title: "Componentes/Section/Title", component: TitleSection };

export const basic = () => (
  <div style={{ width: 300 }}>
    <TitleSection>Título da seção</TitleSection>
  </div>
);
