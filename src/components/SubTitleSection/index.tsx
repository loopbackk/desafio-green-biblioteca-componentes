import React from "react";
import * as Styled from "./styles";

export const SubTitleSection = ({ children = null, ...props }) => {
  return <Styled.Base {...props}>{children}</Styled.Base>;
};

SubTitleSection.displayName = "SubTitleSection";
