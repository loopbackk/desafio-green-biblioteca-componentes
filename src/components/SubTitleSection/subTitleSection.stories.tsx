import React from "react";
import { SubTitleSection } from "./index";

export default { title: "Componentes/Section/SubTitle", component: SubTitleSection };

export const basic = () => (
  <div style={{ width: 300 }}>
    <SubTitleSection>Subtítulo da seção</SubTitleSection>
  </div>
);
