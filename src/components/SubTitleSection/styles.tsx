import styled from "styled-components";
import tokens from "~tokens";

export const Base = styled.h3`
  color: ${tokens.colorNeutralDark};
`;
