import React from "react";
import { render, screen } from "@testing-library/react";
import { SubTitleSection } from "./";

test("renderização correta do texto e estilo", async () => {
  render(
    <SubTitleSection data-testid={"subtitlesection"}>Subtítulo da seção</SubTitleSection>
  );

  const input = screen.getByTestId("subtitlesection");

  expect(input).toHaveTextContent("Subtítulo da seção");
  expect(input).toHaveStyle("color: rgb(93, 97, 95);");
});
