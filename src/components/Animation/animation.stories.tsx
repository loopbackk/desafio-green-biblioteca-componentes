import React from "react";
import { Animation } from "./index";
import * as NewDisclaimerAnimated from "./pacman-loading.json";

export default { title: "Componentes/Animations", component: Animation };

export const basic = () => (
  <Animation fileAnimation={NewDisclaimerAnimated} height={"500px"} width={"500px"} />
);
