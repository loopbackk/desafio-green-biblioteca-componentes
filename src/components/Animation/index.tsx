import React, { ReactNode } from "react";
import Lottie from "react-lottie";

type AnimationProps = {
  height: string;
  width: string;
  fileAnimation: ReactNode;
  loop?: boolean;
  autoplay?: boolean;
  onCompleteAnimation?: () => void;
  preserveAspectRatio?: string;
};

export const Animation = ({
  autoplay = true,
  height,
  width,
  fileAnimation,
  loop = true,
  onCompleteAnimation = () => {},
  preserveAspectRatio = "xMidYMid slice",
}: AnimationProps) => {
  const defaultOptions = {
    loop,
    autoplay,
    renderer: "svg",
    animationData: fileAnimation,
    rendererSettings: {
      preserveAspectRatio,
    },
  };
  return (
    <Lottie
      options={defaultOptions}
      height={height}
      width={width}
      eventListeners={[
        {
          eventName: "complete",
          callback: () => onCompleteAnimation,
        },
      ]}
      isClickToPauseDisabled
    />
  );
};
