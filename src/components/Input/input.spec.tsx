import React from "react";
import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { Input } from "./";

test("entrada do usuário", async () => {
  render(<Input />);

  const input = screen.getByRole("textbox");

  await userEvent.type(input, "hello world");
  expect(input).toHaveValue("hello world");
});

test("mudança de estilo ao desabilitar", async () => {
  render(<Input disabled />);

  const input = screen.getByRole("textbox");

  expect(input).toHaveStyle(`cursor: not-allowed;`);
  expect(input).toHaveStyle(`opacity: 0.8; `);
  expect(input).toHaveStyle(`background-color: rgb(242,242,242);`);
  expect(input).toHaveStyle(`border-color: rgb(217,221,219);`);
  expect(input).toHaveStyle(`color: rgb(153,158,156);`);
});
