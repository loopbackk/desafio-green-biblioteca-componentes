import React from "react";
import { Input } from "./index";

export default { title: "Componentes/Input", component: Input };

export const basic = () => (
  <div style={{ width: 300 }}>
    <Input placeholder="Hello world" />
  </div>
);

export const disabled = () => (
  <div style={{ width: 300 }}>
    <Input placeholder="Hello world" isDisabled />
  </div>
);
