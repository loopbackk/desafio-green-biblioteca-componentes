import styled from "styled-components";
import tokens from "~tokens";

const OPTIONAL_FIELD_WIDTH = 87; // 55px base from the text + 12px (horizontal padding) * 2

const paddingRightMap = {
  none: 0,
  optionalField: OPTIONAL_FIELD_WIDTH - 6, // the full value pushes the text too much to the right
  errorIcon: 48,
};

export const Container = styled.div`
  position: relative;
  display: inline-flex;
  width: 100%;
`;

export const Input = styled.input<{ rightElementType: string }>`
  -webkit-appearance: none;
  -moz-appearance: none;
  appearance: none;
  width: 100%;
  height: 48px;
  padding: 10px 24px;
  box-sizing: border-box;
  transition: border 0.2s ease;
  color: ${tokens.colorNeutralXdark};
  font-size: inherit;
  font-family: inherit;
  outline: 0;
  border: solid 1px ${tokens.colorNeutralLight};
  border-radius: 1px;
  padding-right: ${props => `${paddingRightMap[props.rightElementType]}px`};

  [data-whatintent="mouse"] &:hover,
  &:focus {
    border-color: ${tokens.colorNeutralMedium};
  }

  &::-ms-clear {
    display: none;
  }

  &::placeholder {
    color: ${tokens.colorNeutralMedium};
  }

  &&[disabled] {
    cursor: not-allowed;
    opacity: 0.8;
    background-color: ${tokens.colorBackground03};
    border-color: ${tokens.colorNeutralLight};
    color: ${tokens.colorNeutralMedium};
  }
`;
