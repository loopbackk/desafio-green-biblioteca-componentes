import React from "react";
import { StyledComponentPropsWithRef } from "styled-components";
import * as Styled from "./styles";

export type FormControlBaseProps = {
  isDisabled?: boolean;
};

export type InputProps = StyledComponentPropsWithRef<"input"> &
  FormControlBaseProps & {
    isNumeric?: boolean;
  };

export const Input = React.forwardRef<HTMLInputElement, InputProps>(
  ({ isNumeric, className, isDisabled, ...props }, ref) => {
    const rightElementType = "none";

    const numericProps: InputProps = isNumeric
      ? {
          inputMode: "numeric",
          pattern: "[0-9]*",
        }
      : {};

    return (
      <Styled.Container className={className}>
        <Styled.Input
          disabled={isDisabled}
          rightElementType={rightElementType}
          ref={ref}
          {...numericProps}
          {...props}
        />
      </Styled.Container>
    );
  }
);

Input.displayName = "Input";
