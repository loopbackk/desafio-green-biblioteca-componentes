import React from "react";
import * as Styled from "./styles";

export const Section = ({ children = null, ...props }) => {
  return <Styled.Base {...props}>{children}</Styled.Base>;
};

Section.displayName = "Section";
