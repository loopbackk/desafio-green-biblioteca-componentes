import React from "react";
import { render, screen } from "@testing-library/react";
import { Section } from "./";

test("renderização correta do texto e estilo", async () => {
  render(<Section data-testid={"section"}>Título da seção</Section>);

  const input = screen.getByTestId("section");

  expect(input).toHaveTextContent("Título da seção");
  expect(input).toHaveStyle("margin-bottom: 40px;");
});
