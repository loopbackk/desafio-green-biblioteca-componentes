import React from "react";
import { Section } from "./index";

export default { title: "Componentes/Section/Container", component: Section };

export const basic = () => (
  <div style={{ width: 300 }}>
    <Section>
      <div> Elemento dentro da Section</div>
    </Section>
  </div>
);
