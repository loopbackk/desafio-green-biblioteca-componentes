import React from "react";
import { StyledComponentPropsWithRef } from "styled-components";
import * as Styled from "./styles";
import { AsProp } from "../typeUtils";

export type ButtonColor = "primary" | "orange";

export type ButtonBaseProps = StyledComponentPropsWithRef<"button"> &
  AsProp & {
    color?: ButtonColor;
  };

export const Button = React.forwardRef<HTMLButtonElement, ButtonBaseProps>(
  ({ children = null, onClick = () => {}, color = "primary", ...props }, ref) => {
    const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
      onClick(event);
    };

    return (
      <Styled.Base ref={ref} onClick={handleClick} color={color} {...props}>
        {children}
      </Styled.Base>
    );
  }
);

Button.displayName = "Button";
