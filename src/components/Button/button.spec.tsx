import React from "react";
import { render, screen } from "@testing-library/react";
import { Button } from "./";
import tokens from "~tokens";

test("renderização correta do container e estilo", async () => {
  render(<Button data-testid={"button"}>Teste</Button>);

  const button = screen.getByTestId("button");

  expect(button).toHaveTextContent("Teste");
});

test("mudança de estilo ao desabilitar", async () => {
  render(
    <Button data-testid={"button"} disabled>
      Teste
    </Button>
  );

  const button = screen.getByTestId("button");

  expect(button).toHaveStyle(`cursor: not-allowed;`);
  expect(button).toHaveStyle(`opacity: 0.8; `);
  expect(button).toHaveStyle(`background-color: rgb(242,242,242);`);
  expect(button).toHaveStyle(`border-color: rgb(217,221,219);`);
  expect(button).toHaveStyle(`color: rgb(153,158,156);`);
});

test("mudança de estilo ao passar color='orange'", async () => {
  render(
    <Button data-testid={"button"} color="orange">
      Teste
    </Button>
  );

  const button = screen.getByTestId("button");
  expect(button).toHaveStyle(`background-color: ${tokens.colorBrandSecondaryPure};`);
  expect(button).toHaveStyle(`color: ${tokens.colorBackground04};`);
});
