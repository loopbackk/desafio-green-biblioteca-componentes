import styled from "styled-components";
import tokens from "~tokens";
import { ButtonColor } from ".";

type MapItemButton = {
  color: string;
  backgroundColor: string;
  backgroundColorHover: string;
};

const colorsMap: Record<ButtonColor, MapItemButton> = {
  primary: {
    backgroundColor: tokens.colorBrandPrimaryPure,
    backgroundColorHover: tokens.colorBrandPrimaryDark,
    color: tokens.colorBackground04,
  },
  orange: {
    backgroundColor: tokens.colorBrandSecondaryPure,
    backgroundColorHover: tokens.colorBrandSecondayDark,
    color: tokens.colorBackground04,
  },
};

type StyleButtonProps = {
  color: ButtonColor;
};

export const Base = styled.button<StyleButtonProps>`
  border: none;
  padding: 16px;

  color: ${props => colorsMap[props.color].color};
  background-color: ${props => colorsMap[props.color].backgroundColor};

  &&[disabled] {
    cursor: not-allowed;
    opacity: 0.8;
    background-color: ${tokens.colorBackground03};
    border-color: ${tokens.colorNeutralLight};
    color: ${tokens.colorNeutralMedium};
  }

  :hover {
    background-color: ${props => colorsMap[props.color].backgroundColorHover};
  }
`;
