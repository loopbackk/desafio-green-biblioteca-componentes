import React from "react";
import { Button } from "./index";

export default { title: "Componentes/Botões", component: Button };

export const basic = () => (
  <>
    <Button color={"primary"}>Primary</Button>
    <br />
    <br />
    <Button color={"orange"}>Orange</Button>
  </>
);

export const disabled = () => (
  <>
    <Button color={"primary"} disabled>
      Primary
    </Button>
  </>
);
