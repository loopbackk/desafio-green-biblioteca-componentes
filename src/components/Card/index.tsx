import React from "react";
import * as Styled from "./styles";

export const Card = ({ children = null, ...props }) => {
  return <Styled.Base {...props}>{children}</Styled.Base>;
};

Card.displayName = "Card";
