import styled from "styled-components";
import tokens from "~tokens";

export const Base = styled.div`
  border: solid 2px ${tokens.colorBrandPrimaryLight};
  padding: 10px;
  box-shadow: 2px 2px ${tokens.colorBrandPrimaryLight};
`;
