import React from "react";
import { Card } from "./index";

export default { title: "Componentes/Card", component: Card };

export const basic = () => (
  <div style={{ width: 300 }}>
    <Card>
      <div> Elemento dentro do Card</div>
    </Card>
  </div>
);
