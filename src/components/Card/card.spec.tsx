import React from "react";
import { render, screen } from "@testing-library/react";
import { Card } from "./";

test("renderização correta do container e estilo", async () => {
  render(
    <Card data-testid={"card"}>
      <div>Container Card</div>
    </Card>
  );

  const input = screen.getByTestId("card");

  expect(input).toHaveTextContent("Container Card");
  expect(input).toHaveStyle(`border: solid 2px rgb(84,188,130);`);
  expect(input).toHaveStyle(`padding: 10px;`);
  expect(input).toHaveStyle(`box-shadow: 2px 2px rgb(84,188,130);`);
});
