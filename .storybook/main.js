const path = require('path');

const modulesToReplace = ['colors', 'metrics', 'typography', 'icons', 'tokens'];

const IS_VISUAL_TEST = process.env.STORYBOOK_VISUAL_TESTING === 'true';

module.exports = {
  stories: ['../src/**/*.stories.@(ts|tsx|js|jsx|mdx)'],
  webpackFinal: async config => {
    // Aliases
    modulesToReplace.forEach(module => {
      const alias = `~${module}`;
      const actualPath = path.join(__dirname, '..', 'src', module);

      config.resolve.alias[alias] = actualPath;
    });

    return config;
  },
  typescript: {
    reactDocgen: IS_VISUAL_TEST ? null : 'react-docgen-typescript',
    reactDocgenTypescriptOptions: {
      propFilter: (prop, component) => {
        // Include all the external props from react-modal and tippyjs
        if (
          prop.parent == null ||
          component.name === 'Modal' ||
          component.name === 'Tooltip'
        ) {
          return true;
        }

        // Default filter function
        return prop.parent ? !/node_modules/.test(prop.parent.fileName) : true;
      },
    },
  },
  addons: ['@storybook/addon-actions', '@storybook/addon-links', '@storybook/addon-docs'],
};
