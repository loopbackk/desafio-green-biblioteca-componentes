import React from 'react';
//import 'what-input';
//import Modal from 'react-modal';
import GlobalStyle from '../src/components/GlobalStyle';

export const decorators = [
  storyFn => (
    <>
      <GlobalStyle />
      {storyFn()}
    </>
  ),
];

export const parameters = {
  options: {
    // Sort the stories
    storySort: (a, b) =>
      a[1].kind === b[1].kind ? 0 : a[1].id.localeCompare(b[1].id, { numeric: true }),
  },
};

//Modal.setAppElement('#docs-root');
