# Lib Componentes Desafio Green

## Guia de desenvolvimento

### Setup

- versão do node: `12.18.1`
- versão do yarn: `1.22.10`

- Instale as dependências usando: `yarn install`.

### Building

- para buildar o projeto use o comando: `yarn prepare`.

### Testes unitários

- Para rodar todos os testes use: `yarn test`.
- Biblioteca de testes utilizada: `@testing-library/react`

### Rodar localmente

- Para testar localmente você pode utilizar a biblioteca [yalc](https://www.npmjs.com/package/yalc).
- instale globalmente: `npm i -g yalc`.
- publique o seu projeto localmente: `yalc publish`
- adicione ao seu projeto de utilização: `yalc add lib-components-desafio-green`


### Storybook

- Para disponibilizar os componentes para visualização, foi utilizado o Storybook. o link publico para o desafio foi 
disponibilizado no seguinte link: [storybook desafio](https://www.npmjs.com/package/yalc), por meio do firebase hosting.


- O storybook também é um meio de verificar como os componentes desenvolvidos estão ficando. 
  É necessário apenas rodar o comando: `yarn storybook`.


### Tokens

- Com a biblioteca de componentes também é possível a exportação de padrões para o Design System. 
  No projeto foi disponibilizado algumas cores para utilização.
- Os tokens são criados a partir de arquivos `YAML`, que podem ser adicionados na pasta `src/tokens`
- Depois de adicionados, os mesmos são transformados em código TS (`src/tokens/index.ts`)

### Firebase hosting

- O projeto foi configurado para disponibilizar o storybook por meio do firebase hosting, 
  para isso é necessário buildar o storybook (`yarn build:storybook`), e então disponibilizar 
  no google hosting (`firebase deploy`)
  
### NPM 

- O projeto ficou disponível no NPM para utilização no projeto do desafio. [npm desafio](https://www.npmjs.com/package/lib-components-desafio-green)

### Animações

- O projeto conta com a utilização da biblioteca `react-lottie`, onde é possível utilizar animações por meio do 
  (`Adobe After Effects`), existem muitas [animações](https://lottiefiles.com/featured) que podem ser utilizadas. 